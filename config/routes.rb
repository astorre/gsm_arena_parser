# frozen_string_literal: true
Rails.application.routes.draw do
  root 'phone_parse#index'

  get 'show_brand_phones' => 'phone_parse#show_brand_phones'
  get 'show_phone_info' => 'phone_parse#show_phone_info'
  post 'search_phone' => 'phone_parse#search_phone'
end
