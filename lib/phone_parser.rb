# frozen_string_literal: true
# Phone parser module
module PhoneParser
  def source_site
    'http://www.gsmarena.com/'
  end

  def brands
    { 'Acer' => 'acer-phones-59.php',
      'alcatel' => 'alcatel-phones-5.php',
      'Allview' => 'allview-phones-88.php',
      'Amazon' => 'amazon-phones-76.php',
      'Amoi' => 'amoi-phones-28.php',
      'Apple' => 'apple-phones-48.php',
      'Archos' => 'archos-phones-90.php',
      'Asus' => 'asus-phones-46.php',
      'AT&T' => 'at&t-phones-57.php',
      'Benefon' => 'benefon-phones-15.php',
      'BenQ' => 'benq-phones-31.php',
      'BenQ-Siemens' => 'benq_siemens-phones-42.php',
      'Bird' => 'bird-phones-34.php',
      'BlackBerry' => 'blackberry-phones-36.php',
      'BLU' => 'blu-phones-67.php',
      'Bosch' => 'bosch-phones-10.php',
      'BQ' => 'bq-phones-108.php',
      'Casio' => 'casio-phones-77.php',
      'Cat' => 'cat-phones-89.php',
      'Celkon' => 'celkon-phones-75.php',
      'Chea' => 'chea-phones-24.php',
      'Coolpad' => 'coolpad-phones-105.php',
      'Dell' => 'dell-phones-61.php',
      'Emporia' => 'emporia-phones-93.php',
      'Energizer' => 'energizer-phones-106.php',
      'Ericsson' => 'ericsson-phones-2.php',
      'Eten' => 'eten-phones-40.php',
      'Fujitsu Siemens' => 'fujitsu_siemens-phones-50.php',
      'Garmin-Asus' => 'garmin_asus-phones-65.php',
      'Gigabyte' => 'gigabyte-phones-47.php',
      'Gionee' => 'gionee-phones-92.php',
      'Google' => 'google-phones-107.php',
      'Haier' => 'haier-phones-33.php',
      'HP' => 'hp-phones-41.php',
      'HTC' => 'htc-phones-45.php',
      'Huawei' => 'huawei-phones-58.php',
      'i-mate' => 'i_mate-phones-35.php',
      'i-mobile' => 'i_mobile-phones-52.php',
      'Icemobile' => 'icemobile-phones-69.php',
      'Innostream' => 'innostream-phones-29.php',
      'iNQ' => 'inq-phones-60.php',
      'Intex' => 'intex-phones-102.php',
      'Jolla' => 'jolla-phones-84.php',
      'Karbonn' => 'karbonn-phones-83.php',
      'Kyocera' => 'kyocera-phones-17.php',
      'Lava' => 'lava-phones-94.php',
      'LeEco' => 'leeco-phones-109.php',
      'Lenovo' => 'lenovo-phones-73.php',
      'LG' => 'lg-phones-20.php',
      'Maxon' => 'maxon-phones-14.php',
      'Maxwest' => 'maxwest-phones-87.php',
      'Meizu' => 'meizu-phones-74.php',
      'Micromax' => 'micromax-phones-66.php',
      'Microsoft' => 'microsoft-phones-64.php',
      'Mitac' => 'mitac-phones-25.php',
      'Mitsubishi' => 'mitsubishi-phones-8.php',
      'Modu' => 'modu-phones-63.php',
      'Motorola' => 'motorola-phones-4.php',
      'MWg' => 'mwg-phones-56.php',
      'NEC' => 'nec-phones-12.php',
      'Neonode' => 'neonode-phones-22.php',
      'NIU' => 'niu-phones-79.php',
      'Nokia' => 'nokia-phones-1.php',
      'Nvidia' => 'nvidia-phones-97.php',
      'O2' => 'o2-phones-30.php',
      'OnePlus' => 'oneplus-phones-95.php',
      'Oppo' => 'oppo-phones-82.php',
      'Orange' => 'orange-phones-71.php',
      'Palm' => 'palm-phones-27.php',
      'Panasonic' => 'panasonic-phones-6.php',
      'Pantech' => 'pantech-phones-32.php',
      'Parla' => 'parla-phones-81.php',
      'Philips' => 'philips-phones-11.php',
      'Plum' => 'plum-phones-72.php',
      'Posh' => 'posh-phones-101.php',
      'Prestigio' => 'prestigio-phones-86.php',
      'QMobile' => 'qmobile-phones-103.php',
      'Qtek' => 'qtek-phones-38.php',
      'Sagem' => 'sagem-phones-13.php',
      'Samsung' => 'samsung-phones-9.php',
      'Sendo' => 'sendo-phones-18.php',
      'Sewon' => 'sewon-phones-26.php',
      'Sharp' => 'sharp-phones-23.php',
      'Siemens' => 'siemens-phones-3.php',
      'Sonim' => 'sonim-phones-54.php',
      'Sony' => 'sony-phones-7.php',
      'Sony Ericsson' => 'sony_ericsson-phones-19.php',
      'Spice' => 'spice-phones-68.php',
      'T-Mobile' => 't_mobile-phones-55.php',
      'Tel.Me.' => 'tel_me_-phones-21.php',
      'Telit' => 'telit-phones-16.php',
      'Thuraya' => 'thuraya-phones-49.php',
      'Toshiba' => 'toshiba-phones-44.php',
      'Unnecto' => 'unnecto-phones-91.php',
      'Vertu' => 'vertu-phones-39.php',
      'verykool' => 'verykool-phones-70.php',
      'vivo' => 'vivo-phones-98.php',
      'VK Mobile' => 'vk_mobile-phones-37.php',
      'Vodafone' => 'vodafone-phones-53.php',
      'Wiko' => 'wiko-phones-96.php',
      'WND' => 'wnd-phones-51.php',
      'XCute' => 'xcute-phones-43.php',
      'Xiaomi' => 'xiaomi-phones-80.php',
      'XOLO' => 'xolo-phones-85.php',
      'Yezz' => 'yezz-phones-78.php',
      'Yota' => 'yota-phones-99.php',
      'YU' => 'yu-phones-100.php',
      'ZTE' => 'zte-phones-62.php' }
  end

  def find_like(query)
    parse_all_phones.select { |e| e[:text][query] }
  end

  def nok_doc(path)
    uri = "#{source_site}#{path}"
    res = open uri
    Nokogiri::HTML(res)
  end

  def phone_links(doc)
    doc.css('.makers').css('a').map do |link|
      { value: link['href'], text: link.text.strip }
    end
  end

  def parse_phone_list(path)
    doc = nok_doc(path)
    phone_links(doc)
  end

  def parse_phone_info(path)
    doc = nok_doc(path)
    all_data = [{ versions: doc.css('#specs-list p')&.text }]
    doc.css('#specs-list table').each do |table|
      all_data += table.css('tr').map do |row|
        th = row.css('th')[0]
        ttl = row.css('td.ttl')[0]
        info = row.css('td.nfo')[0]
        { header: th ? th.text : '',
          ttl: ttl ? ttl.text.strip : '',
          info: info ? info.text : '' }
      end
    end
    all_data
  end

  def parse_all_phones
    hydra = Typhoeus::Hydra.new(max_concurrency: 10)
    requests = brands.map do |_k, v|
      request = Typhoeus::Request.new("#{source_site}#{v}", timeout: 3, followlocation: true)
      hydra.queue(request)
      request
    end
    hydra.run

    requests.flat_map do |request|
      doc = Nokogiri::HTML(request.response.body)
      phone_links(doc)
    end
  end
end
