# frozen_string_literal: true
require 'rails_helper'

feature 'Parser management' do
  scenario 'show brand selector' do
    visit root_path
    expect(current_path).to eq root_path
    expect(page).to have_css '#first-block select#brand_name'
    expect(page).to have_css '#second-block input#q'
  end

  scenario 'select phones', js: true do
    visit root_path
    select('Apple', 'iPhone 6s', 'Apple A9 CPU Dual-core 1.84 GHz Twister GPU PowerVR GT7600')
    select('Motorola', 'Moto X Force', 'Qualcomm MSM8994 Snapdragon 810')
  end

  def select(brand, phone, text)
    expect(page).not_to have_select('brand_name', selected: brand)
    find('#brand_name').find(:option, brand).select_option
    expect(page).to have_select('brand_name', selected: brand)
    expect(page).to have_css '#phone-name'
    find('#phone_name').find(:option, phone).select_option
    expect(page).to have_select('phone_name', selected: phone)
    expect(page).to have_content text
  end

  scenario 'draws waiting', js: true do
    visit root_path
    fill_in 'Phone search:', with: 'nexus'
    click_button 'Search'
    expect(page).to have_css('#search-results', text: 'wait, please...')
  end

  scenario 'draws search matched result', js: true do
    visit root_path
    fill_in 'Phone search:', with: 'nexus'
    Capybara.default_max_wait_time = 10
    click_button 'Search'
    expect(page).to have_css('#search-results a', count: 4)
    expect(page).to have_css('#search-results', text: 'Nexus 9')
    expect(page).to have_css('#search-results', text: 'Nexus 6P')
    expect(page).to have_css('#search-results', text: 'Nexus 5X')
    expect(page).to have_css('#search-results', text: 'Nexus 6')
  end

  scenario 'draws "No matches"', js: true do
    visit root_path
    fill_in 'Phone search:', with: 'abcd'
    Capybara.default_max_wait_time = 10
    click_button 'Search'
    expect(page).to have_css('#search-results', text: 'No matches')
  end
end
