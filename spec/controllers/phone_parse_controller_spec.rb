# frozen_string_literal: true
require 'rails_helper'

describe PhoneParseController do
  describe 'GET #index' do
    it 'renders the :index template' do
      get :index
      expect(response).to render_template :index
    end
  end

  describe 'GET #show_brand_phones' do
    let(:phone) { 'lg-phones-20.php' }

    it 'returns a successful 200 response' do
      get :show_brand_phones, params: { select_tag_value: phone }, format: :json
      expect(response).to be_success
    end

    it 'returns brand phones' do
      get :show_brand_phones, params: { select_tag_value: phone }, format: :json
      expect(JSON.parse(response.body)['success']).to match_response_schema('brand', true)
      expect(JSON.parse(response.body)['success'].length).to eq(60)
    end

    context 'incorrect parameter' do
      it 'returns an error if an incorrect parameter was sended' do
        get :show_brand_phones, params: { bad_value: phone }, format: :json
        expect(response).to be_bad_request
        expect(JSON.parse(response.body)['invalid']).to eq('Invalid parameter.')
      end
    end
  end

  describe 'GET #show_phone_info' do
    let(:phone) { 'apple_iphone_6s-7242.php' }

    it 'returns a successful 200 response' do
      get :show_phone_info, params: { select_tag_value: phone }, format: :json
      expect(response).to be_success
    end

    it 'returns phone info' do
      get :show_phone_info, params: { select_tag_value: phone }, format: :json
      expect(JSON.parse(response.body)['success'][1..-1]).to match_response_schema('phone', true)
    end

    context 'incorrect parameter' do
      it 'returns an error if an incorrect parameter was sended' do
        get :show_phone_info, params: { bad_value: phone }, format: :json
        expect(response).to be_bad_request
        expect(JSON.parse(response.body)['invalid']).to eq('Invalid parameter.')
      end
    end
  end

  describe 'POST #search_phone' do
    it 're-renders the :search_phone template' do
      post :search_phone, xhr: true, params: { q: 'phone' }
      expect(response).to render_template :search_phone
    end
  end
end
