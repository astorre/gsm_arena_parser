// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or any plugin's vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file. JavaScript code in this file should be added after the last require_* statement.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require jquery_ujs
//= require turbolinks
//= require_tree .

$(document).on("turbolinks:load", function() {
  $('input#q').val('');
  $('input[type="submit"]').prop('disabled', true);
  $('input[type="text"]').keyup(function() {
    if($(this).val() != '') {
      $('input[type="submit"]').prop('disabled', false);
    }
  });
  $('#brand_name').val('');
  $('#brand_name').change(getBrandPhones);
  $('#search-form form').submit(function(event) {
    $('#search-results').empty();
    drawPreloader('#search-results');
  });
});

function getBrandPhones() {
  if ($('#brand_name option:selected').attr('value').length == 0)
    return;
  $('#phone-info').empty();
  $('#phone-name').remove();
  var brandName = $('#brand_name option:selected').attr('value');

  return $.ajax({
    url: "show_brand_phones",
    data: {
      select_tag_value: brandName
    },
    beforeSend: function() {
      drawPreloader('#selectors');
    },
    error: function() {
      removePreloaders();
      $('#phone-name').remove();
      $('#selectors').append('<div id="phone-name" class="select">Timeout error</div>');
    },
    success: function(data) {
      removePreloaders();
      drawSelect(data.success);
    },
    timeout: 3000
  });
}

function drawPreloader(block) {
  var preloader = '<div class="preloader">wait, please...</div>';
  $(block).append(preloader);
}

function removePreloaders() {
  $('.preloader').remove();
}

function drawSelect(phones) {
  var select = '<div id="phone-name" class="select"><select id="phone_name" name="phone[name]">';
  var l = phones.length;
  select += ('<option value="">Choose phone...</option>');
  for (var i = 0; i < l; i++) {
    select += ('<option value="' + phones[i]['value'] + '">' + phones[i]['text'] + '</option>');
  }
  select += '</select></div>';
  $('#phone-info').empty();
  $('#phone-name').remove();
  $('#selectors').append(select);
  $('#phone_name').change(getPhone);
}

function getPhone() {
  if ($('#phone_name option:selected').attr('value').length == 0)
    return;
  var phoneName = $('#phone_name option:selected').attr('value');
  $('#phone-info').text('');

  return $.ajax({
    url: "show_phone_info",
    data: {
      select_tag_value: phoneName
    },
    beforeSend: function() {
      drawPreloader('#phone-info');
    },
    error: function() {
      removePreloaders();
      $('#phone-info').text('Timeout error');
    },
    success: function(data) {
      removePreloaders();
      drawPhoneInfo(data.success);
    },
    timeout: 3000
  });
}

function drawPhoneInfo(content) {
  var html = '<p>' + content[0].versions + '</p>';
  var l = content.length;
  html += '<table>';
  for (var i = 1; i < l; i++) {
    header = content[i].header;
    ttl = content[i].ttl;
    info = content[i].info;
    html += '<tr><td>' + (header === null ? '' : header) + '</td><td>' +
                         (ttl === null ? '' : ttl) + '</td><td>' +
                         (info === null ? '' : info) + '</td></tr>';
  }
  html += '</table>';
  $('#phone-info').html(html);
  $('#main-container').css({'height':($(document).height())+'px'});
  $('#first-block').css({'height':($('#main-container').height())+'px'});
  $('#second-block').css({'height':($('#main-container').height())+'px'});
}
