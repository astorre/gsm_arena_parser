# frozen_string_literal: true
# Main parse controller
class PhoneParseController < ApplicationController
  include PhoneParser

  def index
    @brand_names = brands
  end

  def show_brand_phones
    brand_page = params[:select_tag_value]

    if brand_page.present?
      parsed_phones = Rails.cache.fetch("phone-list-#{brand_page}", expires_in: 1.hour) do
        parse_phone_list(brand_page)
      end
      render json: { success: parsed_phones }
    else
      render json: { invalid: 'Invalid parameter.' }, status: :bad_request
    end
  end

  def show_phone_info
    phone_page = params[:select_tag_value]

    if phone_page.present?
      parsed_info = Rails.cache.fetch("phone-info-#{phone_page}", expires_in: 1.hour) do
        parse_phone_info(phone_page)
      end
      render json: { success: parsed_info }
    else
      render json: { invalid: 'Invalid parameter.' }, status: :bad_request
    end
  end

  def search_phone
    query = /#{params[:q].strip}/i
    finded = Rails.cache.fetch("search-#{query}", expires_in: 1.hour) do
      find_like(query)
    end
    @search_result = finded.to_json.html_safe
  end
end
